import { defineStore } from "pinia";
import { Notify } from "quasar";
import { toRaw } from "vue";
import Localbase from "localbase";

const db = new Localbase("db");

export const useTodoStore = defineStore("todo", {
  state: () => ({
    todolist: [],
    newTask: "",
    isEditing: false,
  }),

  getters: {
    fetchTodolist() {
      db.collection("todos")
        .get()
        .then((loadedTasks) => {
          this.todolist = loadedTasks;
        });
    },
  },

  actions: {
    addNewTodo() {
      if (this.newTask.trim() === "") {
        this.sendNotify("negative", "Tarefa não pode ser criada vazia");
        return;
      }

      const newTodo = {
        id: new Date().getTime(),
        isDone: false,
        todo: this.newTask,
      };

      db.collection("todos").add(newTodo);
      this.todolist.push(newTodo);
      this.newTask = "";

      this.sendNotify("positive", "Tarefa criada com sucesso!");
    },

    editTodo(task) {
      db.collection("todos")
        .doc({ id: toRaw(task.id) })
        .set(toRaw(task));

      this.toggleEdit();

      this.sendNotify("info", "Tarefa editada!");
    },

    removeTodo(task) {
      db.collection("todos")
        .doc({ id: toRaw(task.id) })
        .delete();

      this.todolist = toRaw(this.todolist).filter((t) => t !== toRaw(task));

      this.sendNotify("negative", "Tarefa excluída!");
    },

    replaceTodolist(newTodolist) {
      db.collection("todos").set(newTodolist);
    },

    toggleEdit() {
      this.isEditing = !this.isEditing;
    },

    sendNotify(type, message) {
      Notify.create({
        type: type,
        message: message,
      });
    },
  },
});
