# Todo-List Quasar (todolist-quasar)

A todolist app made with Quasar. PWA-ready.

Features:

- A task list displaying all entries.
- Form to add new tasks.
- Options to edit and delete tasks directly from the list.

## Pre requisites

- Node.js v18.17.0 or newer

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
